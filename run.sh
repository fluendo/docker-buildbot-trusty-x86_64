#!/bin/bash

# Only run this script as buildbot user
# we do this here instead of the Dockerfile to give time
# on the system to mount NSS new configuration from the
# host
user=`whoami`
if [ -z $user ]; then
    echo "Something is wrong with the LDAP/NSS"
    exit 1
fi

if [ "$user" = "root" ]; then
    echo "Running this command as buildbot user"
    su -c ./run.sh buildbot
    exit 0
fi

if [ "$user" != "buildbot" ]; then
    echo "This command must be run as root"
    exit 1
fi

# Check envvars
if [ -z ${BUILDMASTER} ]; then
    echo "You must specify the BUILDMASTER envvar"
    exit 1
fi

if [ -z ${BUILDMASTER_PORT} ]; then
    BUILDMASTER_PORT="9076"
fi

if [ -z ${WORKERNAME} ]; then
    WORKERNAME="example-worker"
fi

if [ -z ${WORKERPASS} ]; then
    WORKERPASS="example-pass"
fi

# create the home in case it does not exist
if [ ! -d $HOME ]; then
    sudo mkdir $HOME
fi

owner="$(stat --format '%U' "$HOME")"
if [ "$owner" != "buildbot" ]; then
    sudo chown buildbot $HOME
fi

# create the worker info
UBUNTU_VERSION=`lsb_release -cs`
WORKER_ARCH=`uname -m`
if [ "x$WORKER_ARCH" == "xi686" ]; then
BUILDBOT_WORKER="docker-buildbot-cerbero-${UBUNTU_VERSION}_x86"
else
BUILDBOT_WORKER="docker-buildbot-cerbero-${UBUNTU_VERSION}_x86_64"
fi
mkdir -p /worker/info
echo "Running docker image: $BUILDBOT_WORKER" > /worker/info/host

buildbot-worker create-worker . ${BUILDMASTER}:${BUILDMASTER_PORT} ${WORKERNAME} ${WORKERPASS}
/usr/local/bin/buildbot-worker start --nodaemon
